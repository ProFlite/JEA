Function New-EndpointConfiguration {
    [cmdletbinding()]
    Param (
        [Alias('Path')]
        [string]$FolderPath = '.',
        [ValidateSet('ReadOnly','BreakGlass','Patching','SoftwareInventory')]
        [string[]]$Endpoint
    )

    If (-Not (Test-Path -Path $FolderPath)) {
        $null = mkdir -Path $FolderPath
    }
    ForEach ($Configuration in $Endpoint) {
        $ConfigurationFile = "$FolderPath\${Configuration}.pssc"
        & "$PSScriptRoot\Templates\${Configuration}.ps1" | Out-File -FilePath $ConfigurationFile
        Get-Item -Path $ConfigurationFile
    }
}

Function Get-SoftwareInventory {
    <#
    .SYNOPSIS
    Short description
    
    .DESCRIPTION
    Long description
    
    .EXAMPLE
    An example
    
    .NOTES
    General notes
    #>
    [cmdletbinding()]
    Param ()

    $Paths = 'HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\', 'HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\'

    ForEach ($Path in $Paths) {
        Get-ChildItem -Path $Path | ForEach-Object -Process {
            Get-ItemProperty -Path $PSItem.PSPath | Select-MyObject -Property DisplayName,DisplayVersion
        }
    }
}

Export-ModuleMember -Function *