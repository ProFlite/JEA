@"
@{

# Version number of the schema used for this document
SchemaVersion = '2.0.0.0'

# ID used to uniquely identify this document
GUID = '$((New-Guid).Guid)'

# Author of this document
Author = 'STLPSUG'

# Description of the functionality provided by these settings
Description = 'Endpoint for retrieving software info'

# Session type defaults to apply for this session configuration. Can be 'RestrictedRemoteServer' (recommended), 'Empty', or 'Default'
SessionType = 'RestrictedRemoteServer'
ExecutionPolicy = 'Unrestricted'
# Directory to place session transcripts for this session configuration
TranscriptDirectory = 'C:\Transcripts\SoftwareInventory'

# Whether to run this session configuration as the machine's (virtual) administrator account
RunAsVirtualAccount = `$true
RunAsVirtualAccountGroups = 'Administrators'

# User roles (security groups), and the role capabilities that should be applied to them when applied to a session
RoleDefinitions = @{
    "COMPANY\IT"        = @{ RoleCapabilities = 'SoftwareInventory' }
} 

}
"@